<?php

namespace C4U\Ares;

class AresDownloader {

	private $url;

	const SCRIPTNAME_BASIC = 'darv_bas.cgi?ico=';
	const SCRIPTNAME_STD = 'darv_std.cgi?ico=';

	public function __construct($url) {
		$this->url = $url;
	}

	public function getBasic($ic) {
		$url = $this->url . self::SCRIPTNAME_BASIC . $ic;

		$result = file_get_contents($url);
		if ($result) {
			return $this->getResponse($result);
		}
		return null;
	}

	private function getResponse($result) {
		$xml = simplexml_load_string($result);
		if ($xml) {
			$response = $xml->xpath('/are:Ares_odpovedi/are:Odpoved');
			if (count($response) > 0) return $response[0];
		}
		throw new \Exception('Result not found');
	}

}