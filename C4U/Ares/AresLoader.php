<?php

namespace C4U\Ares;

use C4U\Ares\Entity\CustomerEntity;

class AresLoader {

	private $aresDownloader;

	public function __construct(
		AresDownloader $aresDownloader
	) {
		$this->aresDownloader = $aresDownloader;
	}

	public function loadForIc($ic) {
		$response = $this->aresDownloader->getBasic($ic);
		return $this->mapBasic($response);
	}

	private function mapBasic(\SimpleXMLElement $response) {
		$customerEntity = new CustomerEntity();
		$customerEntity->ic = $this->getFirstElement($response, 'D:VBAS/D:ICO');
		$customerEntity->dic = $this->getFirstElement($response, 'D:VBAS/D:DIC');
		$customerEntity->name = $this->getFirstElement($response, 'D:VBAS/D:OF');

		$customerEntity->source = $this->getFirstElement($response, 'D:VBAS/D:PF/D:NPF');
		$customerEntity->street = $this->getFirstElement($response, 'D:VBAS/D:AA/D:NU');
		$customerEntity->street .= " " . $this->getFirstElement($response, 'D:VBAS/D:AA/D:CD');
		$customerEntity->zip = $this->getFirstElement($response, 'D:VBAS/D:AA/D:PSC');
		$customerEntity->city = $this->getFirstElement($response, 'D:VBAS/D:AA/D:N');

		$addressText = $this->getFirstElement($response, 'D:VBAS/D:AA/D:AT');
		if ($addressText) {
			$e = explode(', ', $addressText);
			if (isset($e[0])) $customerEntity->street = trim($e[0]);
			if (count($e) == 2) {
				$customerEntity->zip = substr($e[1], 0, 6);
				$customerEntity->city = trim(substr($e[1], 6));
			} else if (count($e) == 3) {
				$customerEntity->zip = substr($e[2], 0, 6);
				$customerEntity->city = trim(substr($e[2], 6)) . ' - ' . trim($e[1]);
			}
		}

		$customerEntity->employees = $this->getFirstElement($response, 'D:VBAS/D:KPP');

		return $customerEntity;
	}

	private function getFirstElement(\SimpleXmlElement $element, $xpath) {
		$found = @$element->xpath($xpath);
		if (count($found) <= 0) return null;
		return $found[0] instanceof \SimpleXMLElement ? $found[0]->__toString() : null;
	}

}
 